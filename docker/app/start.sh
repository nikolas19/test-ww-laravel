#!/usr/bin/env sh

if [ ! -d "vendor" ]; then
  echo "===> Running Composer"
  composer install
fi

if [ ! -d "node_modules" ]; then
  echo "===> Running Yarn"
  yarn install
fi

echo "===> Building front-end assets"
npm run dev

echo "===> Waiting for database connection"
if ! dockerize -wait tcp://mysql:3306 -timeout 120s ; then
  echo "Couldn't establish connection to MySQL database"
  exit 1
fi

echo "===> Running migrations"
php artisan migrate

echo "===> Initializing php-fpm"
php-fpm
